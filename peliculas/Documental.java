package activitat5.peliculas;

import activitat5.util.Fecha;
import activitat5.util.Formato;

public class Documental extends Produccion{

    protected String investigador;
    protected String tema;


    public Documental(String titulo, int duracion, String descripcion, Fecha fecha, String investigador, String tema, Formato formato) {
        super(titulo, duracion, descripcion, fecha,formato);
        this.investigador = investigador;
        this.tema = tema;
        this.ser="Documental";
    }

    @Override
    public String toString() {
        String toStringPadre = super.toString();
        return toStringPadre+",investigador: = "+this.investigador+", tema= "+this.tema;
    }

    @Override
    public void mostrarDetalle() {
        super.mostrarDetalle();
        System.out.println("Investigador: "+this.investigador);
        System.out.println("Duración: "+duracion[0]+"h "+duracion[1]+"min "+duracion[2]+"s");
        System.out.println("--------------------------------------------");
    }
}
