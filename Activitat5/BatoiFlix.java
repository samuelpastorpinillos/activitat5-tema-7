package activitat5;

import activitat5.peliculas.Documental;
import activitat5.peliculas.Pelicula;
import activitat5.peliculas.Serie;
import activitat5.util.Fecha;
import activitat5.util.Formato;

public class BatoiFlix {
    public static void main(String[] args) {

        Fecha peliculaNewsOfTheWorls = new Fecha(0,0,2020);
        Pelicula newsOfTheWorld= new Pelicula("News of the World",7200,"Lorem Ipsum Lorem Impusm Lorem Impsum"
                ,peliculaNewsOfTheWorls,"Tom Hanks","Carolina Betller", Formato.AVI);
        Fecha dreamSongs = new Fecha(0,0,2021);
        Documental peliculaDreamSongs= new Documental("Dream Songs",2400,"Lorem Ipsum Lorem Ipsum Lorem Ipsum",dreamSongs,"Enrique Juncosa","Movimiento Hippie",Formato.MPG);
        Fecha fechaElHacker = new Fecha(0,0,2001);
        Serie elHacker = new Serie("El hacker","Lorem Ipsum",fechaElHacker,20,1,Formato.FLV,3600);


        newsOfTheWorld.mostrarDetalle();
        System.out.println(newsOfTheWorld.toString());
        peliculaDreamSongs.mostrarDetalle();
        System.out.println(peliculaDreamSongs.toString());
        elHacker.mostrarDetalle();
        System.out.println(elHacker.toString());





    }
}
